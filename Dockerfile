FROM maven:3.8.3-adoptopenjdk-8 AS MAVEN_BUILD
MAINTAINER Dung Nv
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn package
FROM openjdk:8-jre
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/assessment-tool.jar /app/
COPY config /app/

ENTRYPOINT ["java", "-jar", "assessment-tool.jar" ,"--spring.config.location=./application.properties"]